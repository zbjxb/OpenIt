using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

// This is the code for your desktop app.

namespace OpenIt
{
    public partial class Form1 : Form/*, IMessageFilter*/
    {
        private Timer checkMouseMoveTimer;
        private const int checkInterval = 200;

        private Point lastPt;
        private bool isMouseMoving = false;
        private int mouseStopCnt = 0;

        private bool isMouseDown = false;
        private Size MouseDownPoint2Location;

        private FConfig[] config = null;

        public Form1()
        {
            InitializeComponent();

            //Application.AddMessageFilter(this);

            LoadConfig();

            checkMouseMoveTimer = new Timer();
            checkMouseMoveTimer.Interval = checkInterval;
            checkMouseMoveTimer.Tick += OnMouseMoveTimer;
            //checkMouseMoveTimer.Enabled = true;
            checkMouseMoveTimer.Start();

            lastPt = Cursor.Position;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Click on the link below to continue learning how to build a desktop app using WinForms!
            System.Diagnostics.Process.Start("http://aka.ms/dotnet-get-started-desktop");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Thanks!");
        }

        //bool IMessageFilter.PreFilterMessage(ref Message m)
        //{
        //    //throw new NotImplementedException();
        //    const int WM_MOUSEMOVE = 0x200;
        //    if (m.Msg == WM_MOUSEMOVE)
        //    {
        //        MouseMove(ref m);
        //    }
        //    return false;
        //}

        void MouseMoving(/*ref Message m*/)
        {
            //MessageBox.Show("Mouse Move!");
            isMouseMoving = true;
            this.Opacity = 0.7;
        }

        void MouseStop()
        {
            if (isMouseMoving)
            {
                mouseStopCnt = 0;
                isMouseMoving = false;
            }
            else
            {
                ++mouseStopCnt;
                //if (mouseStopCnt * checkInterval > 2000)
                if (mouseStopCnt > 2000 / checkInterval)
                {
                    this.Opacity = 0.3;
                }
            }
        }

        //protected override void WndProc(ref Message m)
        //{
        //    const int WM_MOUSEMOVE = 0x200;
        //    switch (m.Msg)
        //    {
        //        case WM_MOUSEMOVE:
        //            MouseMove(ref m);
        //            break;
        //    }
        //    base.WndProc(ref m);
        //}

        void OnMouseMoveTimer(object sender, EventArgs e)
        {
            Point currentPt = Cursor.Position;
            if (!lastPt.Equals(currentPt))
            {
                lastPt = currentPt;

                MouseMoving();
            }
            else
            {
                MouseStop();
            }
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            this.Opacity = 0.9;

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.All;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                String[] files = e.Data.GetData(DataFormats.FileDrop, false) as String[];
                // Copy file from external application 
                foreach (string srcfile in files)
                {
                    string ext = Path.GetExtension(srcfile).ToLower();
                    foreach (FConfig c in config)
                    {
                        if (c.extensions == null || c.extensions.Contains(ext))
                        {
                            System.Diagnostics.Process.Start(c.program, c.param + "\"" + srcfile + "\"");
                            break;
                        }
                    }
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message, " Error ",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void LoadConfig()
        {
            if (File.Exists("config.json"))
            {
                string config_text = File.ReadAllText("config.json");
                config = JsonConvert.DeserializeObject<FConfig[]>(config_text);
            }
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            isMouseDown = true;
            MouseDownPoint2Location = new Size(
                Location.X - MousePosition.X,
                Location.Y - MousePosition.Y);
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            isMouseDown = false;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown)
            {
                Location = MousePosition + MouseDownPoint2Location;
            }
        }
    }

    public class FConfig
    {
        public string[] extensions { get; set; }
        public string program;
        public string param;
    }
}
